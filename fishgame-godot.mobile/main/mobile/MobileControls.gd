extends CanvasLayer

func _input(event):
	if ( event is InputEventScreenTouch or event is InputEventScreenDrag ) and $DPad.is_pressed() :
		var move_position = event.position - ( $DPad.position + Vector2(81.5, 81.5))
		$InnerCircle.position = event.position
		$InnerCircle.visible = true
		if move_position.x < 155 :
			if move_position.x > 30 :
				Input.action_press("player1_right")
			else :
				Input.action_release("player1_right")
			if move_position.x < -30 :
				Input.action_press("player1_left")
			else :
				Input.action_release("player1_left")
			if move_position.y > 30 :
				Input.action_press("player1_down")
			else :
				Input.action_release("player1_down")
			if move_position.y < -30 :
				Input.action_press("player1_jump")
			else :
				Input.action_release("player1_jump")
	
	if event is InputEventScreenTouch and event.pressed == false :
		$InnerCircle.visible = false
		Input.action_release("player1_right")
		Input.action_release("player1_left")
		Input.action_release("player1_down")
		Input.action_release("player1_jump")
