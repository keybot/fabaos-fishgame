// create an express app
const express = require("express")
const path = require("path")

const app = express()
const port = process.env.PORT || 3000

// use the express-static middleware
app.use(express.static(__dirname + "/"))

// define the first route
app.get("/", (req, res) => {
    res.sendFile(path.join(__dirname + "/index.html"))
})

// start the server listening for requests
app.listen(port,
    (err) => {
        if (err)
            console.log("Error in server setup")
        else
            console.log("Server listening on Port "+ port)
    }
)